package proxy;

/**
 * Created by 984493
 * on 4/11/2015.
 */

public interface Thing {
    public void compute(String name);
}
