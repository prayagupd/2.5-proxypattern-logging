package proxy;

/**
 * Created by 984493
 * on 4/11/2015.
 */

public class aThing implements Thing {
    private String name;

    public aThing(String name){
        this.name = name;
    }

    public void compute(String name) {
        System.out.println(name + " " + this.name);
    }
}
