package proxy;

/**
 * Created by 984493
 * on 4/11/2015.
 */

public class LogProxy implements Thing {
    Thing thing;

    public LogProxy(Thing thing){
        this.thing = thing;
    }

    public void before(){
        System.out.println("before delegation");
    }

    public void after(){
        System.out.println("after delegation");
    }

    public void compute(String name) {
        before();
        thing.compute(name);
        after();
    }
}
