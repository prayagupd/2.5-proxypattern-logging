package proxy;

/**
 * Created by 984493
 * on 4/11/2015.
 */

//Expected result lines : 4
//        Hello A
//        before delegation
//        Hello A
//        after delegation

public class ProxyMain {
    public static void main(String[] args){
        //3.b
        Thing thing = new aThing("A");
        thing.compute("Hello");

        //3.c
        thing = new LogProxy(thing);
        thing.compute("Hello");


    }
}
